
// bài 1: Xây dựng ứng dụng đổi màu ngôi nhà

// hằng số 
const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

// hàm load màu lên các button
let renderColor = () => {
    contentHtml = ""
    for (i = 0; i < colorList.length; i++) {
        contentHtml += `
        <button class="color-button ${colorList[i]}" onclick="changeColor(${i})">${i}.${colorList[i]}</button>
        `;
    }
    document.getElementById("colortable").innerHTML = contentHtml;
}
// load hàm lên button
renderColor()

// thay đổi màu ngôi nhà

let changeColor = (a) => {
    document.querySelector(".wall").className = `wall ${colorList[a]}`;
}



// bài 2: Tính điểm trung bình

// hàm lấy giá trị
let laySo = () => {
    let a1 = Number(document.getElementById("a1").value);
    let a2 = Number(document.getElementById("a2").value);
    let a3 = Number(document.getElementById("a3").value);
    let b1 = Number(document.getElementById("b1").value);
    let b2 = Number(document.getElementById("b2").value);
    let b3 = Number(document.getElementById("b3").value);
    let b4 = Number(document.getElementById("b4").value);
    return a1, a2, a3, b1, b2, b3, b4;
}

// hàm tính tổng
let tinhTong = (...respram) => {
    laySo();

    let Tong = 0;
    for (i = 0; i < respram.length; i++) {
        Tong += Number(respram[i].value);
    }

    if (respram.length == 3) {
        document.getElementById("Ketqua1").innerHTML = Tong / respram.length;
    } else {
        document.getElementById("Ketqua2").innerHTML = Tong / respram.length;
    }

    return Tong;

}


// bài 3: Xây dựng hiệu ứng Jump text khi hover 
let Text3 = document.querySelector("#baitap3 > h1").innerHTML;

let arrText3 = [...Text3];

console.log(arrText3);

// tạo hàm in chữ
let renderText3 = () => {
    contentHtml = ""
    for (i = 0; i < arrText3.length; i++) {
        contentHtml += `
        <span onmouseover="bigImg(this)" onmouseout="normalImg(this)">${arrText3[i]}</span>
        `;
    }
    document.getElementById("baitap3").innerHTML = contentHtml;
}


// load chữ hoverMe!
renderText3();

// tạo function khi hover vào thẻ
let bigImg = (x) => {
    x.style.fontSize = "400px";
    x.style.transform = "rotate(30deg)";
}

let normalImg = (x) => {
    x.style.fontSize = "200px";
    x.style.transform = "rotate(0deg)";
}